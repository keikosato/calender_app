class EventsController < ApplicationController
  before_action :set_event, only: [:show, :edit, :update, :destroy]
  
  def index
    @events = Event.all
  end
 
  def new
    @events = Event.new
    respond_to do |format|
      format.html
      format.js
    end
  end
 
  def create
    @events = Event.all
    @event = Event.new(event_params)
    respond_to do |format|
      if @event.save
        format.html
        format.js
      else
        format.js {render :new}
      end
    end
  end

  def destroy
    @event.destroy
    respond_to do |format|
      format.html { redirect_to events_url, notice: 'Event was successfully destroyed.' }
      format.json { head :no_content }
    end
  end
 

  private
  # Use callbacks to share common setup or constraints between actions.
  def set_event
    @event = Event.find(params[:id])
  end

  def event_params
    params.require(:event).permit(:title, :memo, :start_date, :end_date)
  end
end

