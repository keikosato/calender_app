// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, or any plugin's
// vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file. JavaScript code in this file should be added after the last require_* statement.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require rails-ujs
//= require jquery
//= require jquery_ujs
//= require jquery.turbolinks
//= require turbolinks
//= require moment
//= require fullcalendar
//= require fullcalendar/locale-all
//= require_tree .



$(document).ready(function() {
  $('#calendar').fullCalendar({
    header: {
      left: 'prev,next today',
      center: 'title',
      right: 'month,agendaWeek,agendaDay'
    },
    events: '/events.json',
    editable: true,
    navLinks: true,
    selectable: true,
    selectHelper: true,
    select: function(start, end) {

      var close = document.getElementById('close');
      var modal = document.getElementById('modal');
      var mask = document.getElementById('mask');

      console.log(start)
      $.getScript('/events/new', function() {
        modal.classList.remove('hidden');
        mask.classList.remove('hidden');

      });

      close.addEventListener('click', function() {
        modal.classList.add('hidden');
        mask.classList.add('hidden');
       });

       mask.addEventListener('click', function() {
        close.click();
       });

       close.onclick = function() {
        location.reload();
      };
    
      
        var title;
        var eventData;
       if (title) {
        eventData = {
          title: title,
          start: start,
          end: end
        };
        $('#calendar').fullCalendar('renderEvent', eventData, true);
      }
      $('#calendar').fullCalendar('unselect');
    },
});
});

